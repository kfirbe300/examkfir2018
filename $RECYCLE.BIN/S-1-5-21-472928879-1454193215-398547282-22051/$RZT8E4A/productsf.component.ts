import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../products/products.service";

@Component({
  selector: 'app-productsf',
  templateUrl: './productsf.component.html',
  styleUrls: ['./productsf.component.css']
})
export class UsersfComponent implements OnInit {

  products;
  
    constructor(private service:ProductsService) { }

    ngOnInit() {

      this.service.getProductsFire().subscribe(response=>
       {console.log(response);
          this.products= response;
     
        })
    }
  }