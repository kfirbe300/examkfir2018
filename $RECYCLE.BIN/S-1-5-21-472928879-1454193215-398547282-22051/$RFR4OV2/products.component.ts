import { Component, OnInit } from '@angular/core';
import { ProductsService } from "./products.service";

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;
  productsKeys=[];
 constructor(private service:ProductsService) {
    //let service = new UsersService();
    //this.spinnerService.show();
    service.getProducts().subscribe(response=>{
      //console.log(response.json());
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
      //this.spinnerService.hide();
    });
   }
   
  
  ngOnInit() {
  }

}
