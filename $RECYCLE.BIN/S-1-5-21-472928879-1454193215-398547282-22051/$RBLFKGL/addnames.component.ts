
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup , FormControl, Validators } from '@angular/forms';
import { NG_VALIDATORS, Validator, ValidationErrors } from '@angular/forms';
import { ProductsService } from "../products.service";
@Component({
  selector: 'addnames',
  templateUrl: './addnames.component.html',
  styleUrls: ['./addnames.component.css']
})
export class AddnamesComponent implements OnInit {

  @Output() addName:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addNamePs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event

  service:ProductsService;
  addname = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      name:new FormControl(""),
      price:new FormControl(""),
  });

  sendData(){
    this.addName.emit(this. addname.value.name); //ברגע שהיוזר לחץ על סנד תשלח למסאגס הודעה שאומרת לו תציג- זאת אומרת תשתמש באווטפוט שהגדרנו למעלה,האירוע שהתקיים זה האד מסאגז
    console.log(this. addname.value);//לוקח את הערכים שהזנתי בפורם ושולח אותם לקונסול. עם אפ12
    this.service.postProduct(this. addname.value).subscribe(//שמירה ב דאטא בייס
    response =>{
      console.log(response.json())
      this.addNamePs.emit();//רקמ מתזמן את האירוע בניגוד לאופטימי שמעדכן מידע
    }

  )

  }

  constructor(service:ProductsService) {
    this.service = service;

   }

  ngOnInit() {
    /*setTimeout(function(){
      location.reload(); 
    },10000);*/
    
  }

}