import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl,FormBuilder,} from '@angular/forms';
import { ActivatedRoute,Router,ParamMap } from '@angular/router';


@Component({
  selector: 'updateform',
  templateUrl: './updateform.component.html',
  styleUrls: ['./updateform.component.css']
})
export class UpdateformComponent implements OnInit {


  @Output() addUser:EventEmitter <any> = new EventEmitter <any>(); // -מגדירים את שם המשתנה בשם אווטפוט מסוג איונט אמיטר כאשר מבצעים השמה של אמיטר חדש ובכך בנינו תשתית של העברת מידע מהבן שהוא מסאג' פורם לאב שהוא מסאגס
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>(); //pasemistic event
  
  service:UsersService;
  userform = new FormGroup({//בנייה של מבנה נתונים בקוד שמתאים לטופס
      username:new FormControl(""),
      email:new FormControl(""),
      
  });

  sendData(){
    this.addUser.emit(this.userform.value.username);

    console.log(this.userform.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.userform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.addUserPs.emit();
          this.router.navigateByUrl("/users")//חזרה לדף הבית
        }
      );
    })
    
  }
  id;
  username;
  email;
  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute, private router: Router) {   	    
     this.service = service;    
     this.route.paramMap.subscribe((params: ParamMap) => {
       this.id = +params.get('id'); // This line converts id from string into num      
       this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json();                                
        this.user = response.json();
        console.log(this.user.name);
        this.username = this.user.username
        this.email = this.user.email                                
       });      
     });
   }

   user;
   ngOnInit() {
     this.route.paramMap.subscribe(params=>{
       let id = params.get('id');
       console.log(id);
       this.service.getUser(id).subscribe(response=>{
         this.user = response.json();
         console.log(this.user);
       })
   })
   }
   
   }