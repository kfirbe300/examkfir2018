import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';//לקלוט את איי דיי מהראוטר
import {UsersService} from './../users.service'

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
user;
constructor(private route: ActivatedRoute, private service:UsersService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}