import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
  http:Http;
  
  getUsersFire() {
        
        return this.db.list('/users').valueChanges();
     }
  postUser(data){//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('username',data.username).append('email',data.email);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post(environment.url+'users',params.toString(),options);
  }
  getUser(id){
    
    let response =  this.http.get(environment.url+'users/'+id);
    return response;
  }

  deleteUser (key){
    return this.http.delete(environment.url+'users/'+key)
  }
  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.put(environment.url+'users/'+ key,params.toString(), options);
  }
  
  getProducts(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return this.http.get(environment.url+'users');
  }
  

  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('username',user.username).append('email',user.email);
    return this.http.put(environment.url + 'users/'+id, params.toString(), options);      
  }

login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('username', credentials.username).append('password',credentials.password);
    return this.http.post(environment.url+'login', params.toString(),options).map(response=>{ 
      let success = response.json().success;
      if (success == true){
       localStorage.setItem('auth','true');
      }else{
        localStorage.setItem('auth','false');        
      }
   });
 }





  constructor(http:Http,private db:AngularFireDatabase) { 
    this.http = http;
  }
}