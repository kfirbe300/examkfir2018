import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UserslistComponent implements OnInit {
usersform = new FormGroup({
    username: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required)
  
  });
  usersformupdate = new FormGroup({
    username: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required)
  });
  
  showSlim:Boolean = true;
  users;
  usersKeys = [];
  updates = [];
  lastOpenedToUpdate;
  fusers;

  sendData(){
    if(this.usersform.invalid) return;
    this.service.postUser(this.usersform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });
  }
  deleteUser(id){
      console.log(id);
      this.service.deleteUser(id).subscribe(response=>{
        this.service.getUsers().subscribe(response=>{
          this.users =  response.json();
          this.usersKeys = Object.keys(this.users);         
        })
      })
  }

  showUpdate(key){
    if(this.updates[key]){
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){
        this.updates[this.lastOpenedToUpdate] = false;
      }
      this.updates[key] = true;
      this.usersformupdate.get('username').setValue(this.users[key].username);
      this.usersformupdate.get('email').setValue(this.users[key].email);
      this.lastOpenedToUpdate = key;      
    } 
  }
  
  updateUser(id){
    if(this.usersformupdate.invalid) return;
    this.service.updateUser(id,this.usersformupdate.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });    
  }
  
  
  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });
    this.service.getUsersFire().subscribe(fusers =>{
      this.fusers = fusers;
      console.log(this.fusers);
    });    
  }

}