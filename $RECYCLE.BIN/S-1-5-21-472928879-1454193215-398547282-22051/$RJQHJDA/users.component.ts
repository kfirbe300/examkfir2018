import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Router } from "@angular/router"; //Login without JWT


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  products;
  productsKeys=[];

pasemisticAdd(){
    this.service.getProducts().subscribe(response=>{
          //console.log(response.json())//arrow function. .json() converts the string that we recieved to json
          this.products = response.json();
          this.productsKeys = Object.keys(this.products);
        
      });
        
    } 


  constructor(private service:UsersService, private router:Router) {
    //let service = new UsersService();
    service.getProducts().subscribe(response=>{
      //console.log(response.json());
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
    });
   }
   optimisticAdd(product){//user= $event <<------מהאי טי מל
    //console.log("addUser worked " + user);
    var newKey =  parseInt(this. productsKeys[this.productsKeys.length-1],0) +1;
    var newUserObject ={};//מגדיר שזה אובייקט כי זה מערך של אובייקטים
    newUserObject['username'] = product;
    this.products[newKey] = newUserObject;
    this.productsKeys = Object.keys(this.products);//לוקח את מסאג' קייז ומוסיף לו את האיבר שהוספנו במערך
      
  }
  
  //Login without JWT
  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }
    
     deleteUser(key){
    console.log.apply(key);
    let index = this.productsKeys.indexOf(key);
    this.productsKeys.splice(index,1);
    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }
   
    
  ngOnInit() {
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     this.router.navigate(['/users']);
    }else{
      this.router.navigate(['/login']);
    }  
   }
 
 }
