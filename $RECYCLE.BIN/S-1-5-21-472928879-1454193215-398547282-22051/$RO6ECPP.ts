import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class ProductService {
  http:Http;
  
  postProducts(data)
  {//השיטה תקבל קובץ גייסון ותחליף אותו
    let options =  {
      headers:new Headers({//הגדרנו דרך מחלקה מיוחדת של אנגולר  שנקראת הדרס שליחה של קי וואליו
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('product',data.product);//פאראמס הוא למעשה מבנה נתונים שמחזיק קי ו- ואליו, קי הוא המאסג' והואליו הו א הדאטא.מסאג
     return this.http.post('http://localhost/angular/slim/products',params.toString(),options);
  }
  getProducts(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/angular/slim/products');
  }
  
  constructor(http:Http) { 
    this.http = http;
  }
}